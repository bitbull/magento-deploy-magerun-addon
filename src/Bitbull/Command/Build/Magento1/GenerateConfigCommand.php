<?php
/**
 * @author Gennaro Vietri <gennaro.vietri@bitbull.it>
 */
namespace Bitbull\Command\Build\Magento1;

use Bitbull\Utils;
use Dotenv\Dotenv;
use N98\Magento\Command\AbstractMagentoCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;


class GenerateConfigCommand extends AbstractMagentoCommand
{
    /** @var InputInterface $input */
    protected $_input;

    /** @var OutputInterface $output */
    protected $_output;

    //Overwrite conf flag
    const OVERWRITE_CONF = "OVERWRITE_CONF";

    const COMPOSER_VENDOR_PATH = 'COMPOSER_VENDOR_PATH';

    private static $_vars = array(
        "APP_KEY",
        "DB_HOSTNAME",
        "DB_USERNAME",
        "DB_PASSWORD",
        "DB_NAME",
        "REDIS_ENDPOINT",
        "SESSION_SAVE",
        "PSR0_NAMESPACES",
        "ADMIN_FRONTNAME",
        "ENABLE_REDIS_SESSION",
        "ERROR_EMAIL",
        "ENABLE_FPC",
        "DISABLE_MODULES_AUTO_UPDATES",
    );

    private static $_optionals = array(
        "PSR0_NAMESPACES",
        "REDIS_ENDPOINT",
        "ENABLE_REDIS_SESSION",
        "ERROR_EMAIL",
        "ENABLE_FPC",
        "DISABLE_MODULES_AUTO_UPDATES",
    );

    private $_vendorPath = null;

    private $_localTemplate = null;

    /** @var \Bitbull\Utils */
    private $_utils;

    protected function configure()
    {
        $this
            ->setName('build:m1:generate-config')
            ->addOption('composer-vendor-path', null, InputOption::VALUE_OPTIONAL, 'The path of Composer vendor directory', null)
            ->addOption('local-xml-template', null, InputOption::VALUE_OPTIONAL, ' The path of twig template of the local.xml file', null)
            ->setDescription('Generate app/etc/local.xml for current Magento installation based on environment variables')
        ;
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     * @return int|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $config = [];

        $this->_input = $input;
        $this->_output = $output;

        $this->_utils = new Utils();

        $this->detectMagento($output);

        $this->_output->writeln("<info>Generating app/etc/local.xml configuration file for Magento</info>");

        if ($this->_existsLocalConf() && !$this->_overwriteConf()) {
            $this->_output->writeln("<info>Local conf already exists, i will skip it!</info>");
            return;
        }

        $this->_checkInputOptions();
        $this->_loadEnvironmentConfig();
        $config = $this->_addConfigutation($config);

        $this->_generateLocalXMLFile($config);

        $this->_output->writeln("<info>Saved local.xml configuration</info>");
    }

    private function _enableRedisSessionModule()
    {
        $magentoRootPath = realpath($this->_magentoRootFolder);

        $this->_utils->replaceXmlTextNode(
            $magentoRootPath . '/app/etc/modules/Cm_RedisSession.xml',
            '/config/modules/Cm_RedisSession',
            'active',
            'true'
        );

        $this->_output->writeln("<info>Module Cm_RedisSession.xml enabled</info>");
    }

    /**
     * @param string $email Recipient format follows PHP's mail() specification (RFC 2822)
     */
    private function _setErrorMailAddress($email)
    {
        $magentoRootPath = realpath($this->_magentoRootFolder);

        if (file_exists($magentoRootPath . '/errors/local.xml')) {
            $this->_utils->replaceXmlTextNode(
                $magentoRootPath . '/errors/local.xml',
                '/config/report',
                'email_address',
                $email
            );

            $this->_output->writeln("<info>Wrote new email address(es) in errors/local.xml</info>");
        } else {
            $this->_output->writeln("<warning>An email address is configured ($email) but the file errors/local.xml does not exists</warning>");
        }
    }

    private function _checkInputOptions(){

        $this->_vendorPath = $this->_input->getOption('composer-vendor-path');

        $this->_localTemplate = $this->_input->getOption('local-xml-template');

        if ($this->_localTemplate) {
            if (0 !== strpos($this->_localTemplate, DIRECTORY_SEPARATOR)) {
                $this->_localTemplate = exec('pwd'). DIRECTORY_SEPARATOR . $this->_localTemplate;
            }

            if (!file_exists($this->_localTemplate)) {
                throw new \RuntimeException(
                    "Alternative template for local.xml file \"{$this->_localTemplate}\" does not exist"
                );
            }

        } else {
            $this->_localTemplate = 'local.xml.twig';
        }

    }

    private function _loadEnvironmentConfig(){
        $projectPath = dirname($this->_magentoRootFolder);

        if (file_exists($projectPath . '/.env')) {
            $dotenv = new Dotenv($projectPath);
            $dotenv->load();
        } else if (file_exists($this->_magentoRootFolder . '/.env')) {
            $dotenv = new Dotenv($this->_magentoRootFolder);
            $dotenv->load();
        }
    }

    private function _existsLocalConf()
    {
        return (file_exists($this->_getConfigurationFilePath()));
    }

    private function _overwriteConf()
    {
        return (array_key_exists(self::OVERWRITE_CONF, $_SERVER));
    }

    private function _getConfigurationFilePath()
    {
        return $this->_magentoRootFolder . '/app/etc/local.xml';
    }

    /**
     * @return string
     */
    private function _getComposerPathInConfig()
    {
        $path ='';
        if (null != $this->_vendorPath && $this->_magentoRootFolder != dirname($this->_vendorPath)) {
            $path = $this->_vendorPath;
        } else if (array_key_exists(self::COMPOSER_VENDOR_PATH, $_SERVER)) {
            $path = $_SERVER[self::COMPOSER_VENDOR_PATH];
        }
        return $path;
    }

    /**
     * @param array $config
     * @return mixed
     */
    private function _addConfigutation(array $config)
    {
        $config[self::COMPOSER_VENDOR_PATH] = $this->_getComposerPathInConfig();


        foreach (self::$_vars as $var) {
            if (!array_key_exists($var, $_SERVER) && !in_array($var, self::$_optionals)) {
                throw new \RuntimeException("Missing environment variable \"{$var}\"");
            }

            if (array_key_exists($var, $_SERVER)) {
                $this->_output->writeln("Env variable key -> \"$var\": \"$_SERVER[$var]\"");

                switch ($var) {
                    case "PSR0_NAMESPACES":
                        $_SERVER[$var] = explode(',', $_SERVER[$var]);

                        break;
                    case "ENABLE_REDIS_SESSION":
                        if (!($_SERVER["REDIS_ENDPOINT"] === '') && $_SERVER["ENABLE_REDIS_SESSION"] == true) {
                            $this->_enableRedisSessionModule();
                        }
                        break;
                    case "ERROR_EMAIL":
                        if (!($_SERVER["ERROR_EMAIL"] === '')) {
                            $this->_setErrorMailAddress($_SERVER["ERROR_EMAIL"]);
                        }
                        break;
                }

                $config[$var] = $_SERVER[$var];
            }
        }
        return $config;
    }

    /**
     * @param $config
     */
    private function _generateLocalXMLFile($config)
    {
        $configurationContent = $this->_utils->getConfigurationContent($this->_localTemplate, $config);
        $configurationFilepath = $this->_getConfigurationFilePath();
        file_put_contents($configurationFilepath, $configurationContent);
    }
}
