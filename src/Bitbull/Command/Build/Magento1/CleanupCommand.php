<?php
/**
 * @author Gennaro Vietri <gennaro.vietri@bitbull.it>
 */
namespace Bitbull\Command\Build\Magento1;

use Bitbull\Utils;
use N98\Magento\Command\AbstractMagentoCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


class CleanupCommand extends AbstractMagentoCommand
{
    /** @var InputInterface $input */
    protected $_input;

    /** @var OutputInterface $output */
    protected $_output;

    /** @var \Bitbull\Utils */
    private $_utils;

    protected function configure()
    {
        $this
            ->setName('build:m1:cleanup')
            ->addOption('composer-vendor-path', null, InputOption::VALUE_OPTIONAL, 'The path of Composer vendor directory', null)
            ->setDescription('Remove unwanted dirs (like downloader) and files from Magento root directory')
        ;
    }

    /**
     * Remove unwanted dirs (like Magento Connect's) and files
     *
     * @param \Symfony\Component\Console\Input\InputInterface $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     * @return int|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->_utils = new Utils();

        $unwantedDirs = array(
            'downloader',
            'dev',
        );

        $unwantedFiles = array(
            'LICENSE.html',
            'LICENSE.txt',
            'LICENSE_EE.html',
            'LICENSE_EE.txt',
            'LICENSE_AFL.txt',
            'RELEASE_NOTES.txt',
            '.htaccess.sample',
            'install.php',
            'index.php.sample',
            'php.ini.sample',
            'Readme.md',
        );

        $magentoRootDir = $this->getApplication()->getMagentoRootFolder();

        foreach ($unwantedDirs as $dir){
            $dirPath = $magentoRootDir . DIRECTORY_SEPARATOR . $dir;
            if (is_dir($dirPath)) {
                $this->_utils->removeDirRecursive($dirPath);
            }
        }

        foreach ($unwantedFiles as $file){
            $filePath = $magentoRootDir . DIRECTORY_SEPARATOR . $file;
            if (file_exists($filePath)) {
                unlink($filePath);
            }
        }
    }
}