<?php
namespace Bitbull;

use DOMDocument;
use DOMXPath;

class Utils
{
    /**
     * Retrieve path to template for configuration file
     *
     * @param string $fileName
     * @return string
     */
    private function _getConfigurationTplFilePath($fileName)
    {
        return dirname(__FILE__) . '/../templates/' . $fileName;
    }

    /**
     * Retrieve and parse configuration template
     *
     * @param string $fileName
     * @param array $config
     * @return string
     */
    public function getConfigurationContent($fileName, $config)
    {
        $filePath = file_exists($fileName) ? $fileName : $this->_getConfigurationTplFilePath($fileName);

        $template = file_get_contents($filePath);
        $twig = new \Twig_Environment(new \Twig_Loader_String());

        return $twig->render(
            $template,
            $config
        );
    }

    public function removeDirRecursive($dirPath){

        if (file_exists($dirPath) && is_dir($dirPath))
        {
            $recursiveDirectoryIterator = new \RecursiveDirectoryIterator($dirPath, \FilesystemIterator::SKIP_DOTS);
            foreach (new \RecursiveIteratorIterator($recursiveDirectoryIterator, \RecursiveIteratorIterator::CHILD_FIRST) as $path)
            {
                $path->isDir() ? rmdir($path->getPathname()) : unlink($path->getPathname());
            }
            rmdir($dirPath);
        }
    }

    /**
     * Replaces the first occurrence of the specified XPath node with a new text value
     *
     * @param string $xmlFilePath
     * @param string $xPathNodeParent
     * @param string $xPathNode
     * @param string $newValue
     */
    public function replaceXmlTextNode($xmlFilePath, $xPathNodeParent, $xPathNode, $newValue)
    {
        $xml = New DOMDocument();
        $xml->load($xmlFilePath);

        $xpath = new DOMXpath($xml);
        $parentNode = $xpath->query($xPathNodeParent)->item(0);

        $newNode = $xml->createElement($xPathNode);
        $newNode->appendChild($xml->createTextNode((string)$newValue));

        $parentNode->replaceChild($newNode, $xpath->query($xPathNodeParent . '/' . $xPathNode)->item(0));

        // write new config
        file_put_contents($xmlFilePath, $xml->saveXML());
    }
}
